((Drupal) => {
  Drupal.behaviors.event_countdown = {

    /**
     * The interval for the update.
     */
    countdown_interval: undefined,

    /**
     * A summary of found countdowns.
     */
    countdowns: [],

    /**
     * The configuration.
     */
    configuration: {},

    /**
     *
     * @param context
     * @param settings
     */
    attach(context, settings) {

      /** @type {this} */
      let thisModule = Drupal.behaviors.event_countdown;

      if (!context.querySelector('.event-countdown')) {
        return;
      }

      thisModule.configuration = settings.event_countdown.configuration;

      context.querySelectorAll('.event-countdown').forEach(countdown => {
        let entity_id = countdown.getAttribute('data-entity-id');

        if (!(entity_id in settings.event_countdown.events)) {
          return;
        }

        let countdown_settings = settings.event_countdown.events[entity_id];

        // Set the date we're counting down to.
        // Added 3 zeros as there are milliseconds missing from the Drupal field.
        let countdown_timestamp = `${countdown_settings.timestamp}000`;

        thisModule.createCountdown(countdown, countdown_timestamp);
      });
    },

    /**
     *
     * @param countdown
     * @param countdown_timestamp
     */
    createCountdown(countdown, countdown_timestamp) {
      // Register countdown.
      Drupal.behaviors.event_countdown.countdowns.push({
        countdown,
        countdown_timestamp,
      });
    },

    /**
     *
     */
    updateCountdowns() {
      /** @type {this} */
      let thisModule = Drupal.behaviors.event_countdown;

      thisModule.countdowns.forEach(entry => {
        let {countdown, countdown_timestamp} = entry;

        // Get today's date and time.
        let now = new Date().getTime();

        // Find the distance between now and the countdown date.
        let distance = countdown_timestamp - now;

        // Time calculations for days, hours, minutes and seconds.
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Prettify access to the elements.
        let days_element = countdown.querySelector('.event-countdown-days');
        let hours_element = countdown.querySelector('.event-countdown-hours');
        let minutes_element = countdown.querySelector('.event-countdown-minutes');
        let seconds_element = countdown.querySelector('.event-countdown-seconds');

        // If the countdown is over, write some text
        if (distance < 0) {
          countdown.innerHTML = Drupal.t("Published!");
          return;
        }

        // Only update dom if needed.
        if (!days_element.innerText.includes(days)) {
          days_element.innerText = `${days} ${Drupal.formatPlural(days, 'Day', 'Days')}`;
        }
        if (!hours_element.innerText.includes(hours)) {
          hours_element.innerText = `${hours} ${Drupal.formatPlural(hours, 'Hour', 'Hours')}`;
        }
        if (!minutes_element.innerText.includes(minutes)) {
          minutes_element.innerText = `${minutes} ${Drupal.formatPlural(minutes, 'Minute', 'Minutes')}`;
        }
        if (!seconds_element.innerText.includes(seconds)) {
          seconds_element.innerText = `${seconds} ${Drupal.formatPlural(seconds, 'Second', 'Seconds')}`;
        }

        countdown.classList.remove('event-loading');
      });
    }
  }

  let event_countdown = Drupal.behaviors.event_countdown;
  event_countdown.countdown_interval = setInterval(event_countdown.updateCountdowns,1000);
})(Drupal);
