const mix = require('laravel-mix');

mix.js('./src/VanillaCountdown/VanillaCountdown.js', './js');
mix.sass('./src/VanillaCountdown/VanillaCountdown.scss', './css');
