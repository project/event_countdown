/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/VanillaCountdown/VanillaCountdown.js":
/*!**************************************************!*\
  !*** ./src/VanillaCountdown/VanillaCountdown.js ***!
  \**************************************************/
/***/ (() => {

(function (Drupal) {
  Drupal.behaviors.event_countdown = {
    /**
     * The interval for the update.
     */
    countdown_interval: undefined,

    /**
     * A summary of found countdowns.
     */
    countdowns: [],

    /**
     * The configuration.
     */
    configuration: {},

    /**
     *
     * @param context
     * @param settings
     */
    attach: function attach(context, settings) {
      /** @type {this} */
      var thisModule = Drupal.behaviors.event_countdown;

      if (!context.querySelector('.event-countdown')) {
        return;
      }

      thisModule.configuration = settings.event_countdown.configuration;
      context.querySelectorAll('.event-countdown').forEach(function (countdown) {
        var entity_id = countdown.getAttribute('data-entity-id');

        if (!(entity_id in settings.event_countdown.events)) {
          return;
        }

        var countdown_settings = settings.event_countdown.events[entity_id]; // Set the date we're counting down to.
        // Added 3 zeros as there are milliseconds missing from the Drupal field.

        var countdown_timestamp = "".concat(countdown_settings.timestamp, "000");
        thisModule.createCountdown(countdown, countdown_timestamp);
      });
    },

    /**
     *
     * @param countdown
     * @param countdown_timestamp
     */
    createCountdown: function createCountdown(countdown, countdown_timestamp) {
      // Register countdown.
      Drupal.behaviors.event_countdown.countdowns.push({
        countdown: countdown,
        countdown_timestamp: countdown_timestamp
      });
    },

    /**
     *
     */
    updateCountdowns: function updateCountdowns() {
      /** @type {this} */
      var thisModule = Drupal.behaviors.event_countdown;
      thisModule.countdowns.forEach(function (entry) {
        var countdown = entry.countdown,
            countdown_timestamp = entry.countdown_timestamp; // Get today's date and time.

        var now = new Date().getTime(); // Find the distance between now and the countdown date.

        var distance = countdown_timestamp - now; // Time calculations for days, hours, minutes and seconds.

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
        var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
        var seconds = Math.floor(distance % (1000 * 60) / 1000); // Prettify access to the elements.

        var days_element = countdown.querySelector('.event-countdown-days');
        var hours_element = countdown.querySelector('.event-countdown-hours');
        var minutes_element = countdown.querySelector('.event-countdown-minutes');
        var seconds_element = countdown.querySelector('.event-countdown-seconds'); // If the countdown is over, write some text

        if (distance < 0) {
          countdown.innerHTML = Drupal.t("Published!");
          return;
        } // Only update dom if needed.


        if (!days_element.innerText.includes(days)) {
          days_element.innerText = "".concat(days, " ").concat(Drupal.formatPlural(days, 'Day', 'Days'));
        }

        if (!hours_element.innerText.includes(hours)) {
          hours_element.innerText = "".concat(hours, " ").concat(Drupal.formatPlural(hours, 'Hour', 'Hours'));
        }

        if (!minutes_element.innerText.includes(minutes)) {
          minutes_element.innerText = "".concat(minutes, " ").concat(Drupal.formatPlural(minutes, 'Minute', 'Minutes'));
        }

        if (!seconds_element.innerText.includes(seconds)) {
          seconds_element.innerText = "".concat(seconds, " ").concat(Drupal.formatPlural(seconds, 'Second', 'Seconds'));
        }

        countdown.classList.remove('event-loading');
      });
    }
  };
  var event_countdown = Drupal.behaviors.event_countdown;
  event_countdown.countdown_interval = setInterval(event_countdown.updateCountdowns, 1000);
})(Drupal);

/***/ }),

/***/ "./src/VanillaCountdown/VanillaCountdown.scss":
/*!****************************************************!*\
  !*** ./src/VanillaCountdown/VanillaCountdown.scss ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/VanillaCountdown": 0,
/******/ 			"css/VanillaCountdown": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkassets"] = self["webpackChunkassets"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/VanillaCountdown"], () => (__webpack_require__("./src/VanillaCountdown/VanillaCountdown.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/VanillaCountdown"], () => (__webpack_require__("./src/VanillaCountdown/VanillaCountdown.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;