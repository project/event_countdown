<?php

namespace Drupal\event_countdown\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Event Countdown form.
 */
class EventCountdownForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_countdown_event_countdown';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->blockSettings($form, $form_state);


    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  public function blockSettings(&$form, $form_state) {
    $form['block_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Block Settings'),
    ];

    $block_configuration = &$form['block_configuration'];

    $block_configuration['event_timestamp_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Countdown Display'),
      '#description' => $this->t('Determines how the countdown should be displayed.'),
      '#default_value' => 'drupal_settings',
      '#options' => [
        'static' => $this->t('Static'),
        'drupal_settings' => $this->t('Drupal Settings'),
        'js_countdown' => $this->t('JS Countdown'),
        'vue_countdown' => $this->t('Vuetify Countdown'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('message')) < 10) {
      $form_state->setErrorByName('message', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('The message has been sent.'));
    $form_state->setRedirect('<front>');
  }

}
