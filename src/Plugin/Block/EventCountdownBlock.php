<?php

namespace Drupal\event_countdown\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides an event countdown block.
 *
 * @Block(
 *   id = "event_countdown_block",
 *   admin_label = @Translation("Event Countdown"),
 *   category = @Translation("Custom")
 * )
 */
class EventCountdownBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $node_id = $this->configuration['source_element'];
    $node = Node::load($node_id);

    $form['source_element'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#default_value' => $node,
      '#selection_handler' => 'default',
    ];

    $form['block_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Block Settings'),
      '#open' => TRUE
    ];

    $block_configuration = &$form['block_configuration'];
    $block_configuration['event_timestamp_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Countdown Display'),
      '#description' => $this->t('Determines how the countdown should be displayed.'),
      '#default_value' => $this->configuration['block_configuration']['event_timestamp_display'] ?? 'drupal_settings',
      '#options' => [
        'static' => $this->t('Static'),
        'drupal_settings' => $this->t('Drupal Settings'),
        'js_countdown' => $this->t('JS Countdown'),
        'vue_countdown' => $this->t('Vuetify Countdown'),
      ],
    ];

    $form['event_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Event'),
      '#open' => TRUE
    ];

    $event_configuration = &$form['event_configuration'];
    $event_configuration['event_live_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Event is Live'),
      '#description' => $this->t('The message that is shown when the event has gone live.'),
      '#default_value' => $this->configuration['event_configuration']['event_live_message'] ?? 'Event has gone Live!',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['source_element'] = $form_state->getValue('source_element');
    $this->configuration['block_configuration'] = $form_state->getValue('block_configuration');

    \Drupal::messenger()->addError(implode(',',array_keys($form_state->getValues())));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $node_id = $this->configuration['source_element'];
    $node = Node::load($node_id);

    if (
      !method_exists($node, 'hasField') ||
      !$node->hasField('publish_on')
    ) {
      return [
        '#markup' => $this->t("Node doesn't use scheduler publish on feature."),
      ];
    }

    $publish_on = $node->get('publish_on')->getValue();

    if (empty($publish_on)) {
      return [
        '#markup' => $this->t("Published!"),
      ];
    }

    $timestamp = $publish_on[0]['value'];

    switch ($this->configuration['block_configuration']['event_timestamp_display']) {
      case 'static':
        $this->staticTimestamp($build, $node, $timestamp);
        break;
      case 'drupal_settings':
        $this->drupalSettingsTimestamp($build, $node, $timestamp);
        break;
      case 'js_countdown':
        $this->jsCountdownTimestamp($build, $node, $timestamp);
        break;
    }

    return $build;
  }

  /**
   * @param $build
   * @param $node
   * @param $timestamp
   * @return void
   */
  public function staticTimestamp(&$build, $node, $timestamp) {

    $build['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'event-countdown',
          'event-loading',
        ],
        'data-entity-id' => $node->id(),
        'data-entity-timestamp' => $timestamp,
      ],
    ];

    $build['container']['days'] = [
      '#markup' => "<span class='event-countdown-days'></span>",
    ];
    $build['container']['hours'] = [
      '#markup' => "<span class='event-countdown-hours'></span>",
    ];
    $build['container']['minutes'] = [
      '#markup' => "<span class='event-countdown-minutes'></span>",
    ];
    $build['container']['seconds'] = [
      '#markup' => "<span class='event-countdown-seconds'></span>",
    ];
  }

  public function drupalSettingsTimestamp(&$build, $node, $timestamp) {
    $build['#attached']['drupalSettings']['event_countdown']['configuration'] = $this->configuration;
    $build['#attached']['drupalSettings']['event_countdown']['events'][$node->id()] = [
      'type' => 'node',
      'id' => $node->id(),
      'timestamp' => $timestamp,
    ];
  }

  public function jsCountdownTimestamp(&$build, $node, $timestamp) {
    $this->drupalSettingsTimestamp($build, $node, $timestamp);
    $this->staticTimestamp($build, $node, $timestamp);
    $build['#attached']['library'][] = 'event_countdown/js-countdown';
  }


  public function getCacheMaxAge() {
    return 0;
  }

}
